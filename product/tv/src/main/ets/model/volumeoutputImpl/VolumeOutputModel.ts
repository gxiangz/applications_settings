import tvMWAudio from '@ohos.tvmw.audio'
import promptAction from '@ohos.promptAction';
import LogUtil from '../../../../../../../common/utils/src/main/ets/default/baseUtil/LogUtil';

const TAG = "VolumeOutputModel"


export class VolumeOutputModel {

    private static VOLUME_OUTPUT_MODEL: VolumeOutputModel = undefined;

    constructor() {
    }

    public static getVolumeOutputModel() {
        if (VolumeOutputModel.VOLUME_OUTPUT_MODEL == undefined) {
            VolumeOutputModel.VOLUME_OUTPUT_MODEL = new VolumeOutputModel();
        }
        return VolumeOutputModel.VOLUME_OUTPUT_MODEL;
    }

    public async init() {
        LogUtil.info(TAG + ":=====>get outputMode begin");
        await tvMWAudio.getOutputHDMIMode()
            .then((result) => {
                LogUtil.info(TAG + "getOutputHDMIMode:AudioHDMIMode==>" + result);
                let optionName=undefined;
                switch(result){
                     case 0:optionName="解码";break;
                     case 1:optionName="透传";break;
                     case 2:optionName="自动";break;
                }

                AppStorage.SetOrCreate("hdmiOption", optionName);
            })
            .catch((e) => {
                LogUtil.info(TAG + "getOutputHDMIMode fail" + JSON.stringify(e));
                promptAction.showToast({
                    message: "获取HDMI输出参数失败"
                })
            })
        await tvMWAudio.getOutputSPDIFMode()
            .then((result) => {
                LogUtil.info(TAG + "getOutputSPDIFMode:AudioSPDIFMode==>" + result);
                let optionName=undefined;
                switch(result){
                    case 0:optionName="解码";break;
                    case 1:optionName="透传";break;
                }
                AppStorage.SetOrCreate("spdifOption", optionName);
            })
            .catch((e) => {
                LogUtil.info(TAG + "getOutputSPDIFMode fail" + JSON.stringify(e));
                promptAction.showToast({
                    message: "获取SPDIF输出参数失败"
                })
            })
        await  tvMWAudio.getOutNgenerationMode()
            .then((result) => {
                LogUtil.info(TAG + "getOutNgenerationMode:AudioNgenerationMode==>" + result);
                let optionName=undefined;
                switch(result){
                    case 1: optionName="透传7.1";break;
                    case 2:optionName="自动";break;
                    case 3:optionName="透传5.1";break;
                }
                AppStorage.SetOrCreate("degradeOption",optionName);
            })
            .catch((e) => {
                LogUtil.info(TAG + "getOutNgenerationMode fail" + JSON.stringify(e));
                promptAction.showToast({
                    message: "获取次世代降级输出参数失败"
                })
            })
        LogUtil.info(TAG + ":=====>get outputMode end");

    }

    public setOutNgenerationMode(mode: tvMWAudio.AudioNgenerationMode) {
        LogUtil.info(TAG + ":setNgeneration begin...");
        LogUtil.info(TAG + ":input param:===>" + mode);
        tvMWAudio.setOutNgenerationMode(mode)
            .then((v) => {
                LogUtil.info(TAG + "setOutNgenerationMode  success");
            })
            .catch((e) => {
                LogUtil.info(TAG + "setOutNgenerationMode fail" + JSON.stringify(e));
                promptAction.showToast({
                    message: "次世代降级输出参数设置失败"
                })

            })
    }

    public setOutputHDMIMode(mode: tvMWAudio.AudioHDMIMode) {
        LogUtil.info(TAG + ":setHDMI begin...");
        LogUtil.info(TAG + ":input param:===>" + mode);
        tvMWAudio.setOutputHDMIMode(mode)
            .then((v) => {
                LogUtil.info(TAG + "setOutputHDMIMode  success");
            })
            .catch((e) => {
                LogUtil.info(TAG + "setOutputHDMIMode fail" + JSON.stringify(e));
                promptAction.showToast({
                    message: "HDMI输出参数设置失败"
                })
            })
    }

    public setOutputSPDIFMode(mode: tvMWAudio.AudioSPDIFMode) {
        LogUtil.info(TAG + ":setSPDIF begin...");
        LogUtil.info(TAG + ":input param:===>" + mode);
        tvMWAudio.setOutputSPDIFMode(mode)
            .then((v) => {
                LogUtil.info(TAG + "setOutputSPDIFMode  success");
            })
            .catch((e) => {
                LogUtil.info(TAG + "setOutputSPDIFMode fail" + JSON.stringify(e));
                promptAction.showToast({
                    message: "SPDIF输出参数设置失败"
                })
            })
    }
}

let volumeOutputModel = VolumeOutputModel.getVolumeOutputModel();

export {volumeOutputModel }