import { SettingBackground } from '../../default/layout/SettingBackground';
import { Ratio } from '../../common/ratio';
import { SettingItemDesLayout } from '../../default/layout/SettingItemDesLayout';
import router from '@ohos.router';
import LogUtil from '../../../../../../../common/utils/src/main/ets/default/baseUtil/LogUtil';
import ConfigData from '../../../../../../../common/utils/src/main/ets/default/baseUtil/ConfigData';
import { KeyCode } from '../../common/constants/KeyCode';
import WifiModel from '../../model/wifiImpl/WifiModel';
import lpx from '../../common/lpx'

import {
  WifiScanInfo,
  ApScanResult,
  WiFiSummaryMap,
  WiFiIntensityMap,
  WiFiEncryptMethodMap
} from '../../model/wifiImpl/WifiModel';

const TAG = "WifiInfoDetails"

@Entry
@Component
struct WifiInfoDetails {
  @State imageUrl: string = Ratio.resName('icon_wifi')
  @State des: string = ''
  private apInfo: WifiScanInfo;
  private frequency: string = '2.4GHz';
  @State focusIndex: number = -1;
  private speed: string = '144Mbps';
  private isConnected = false;
  @State dataList: any[] = [
    { key: $r('app.string.wifiInfoTitleStatus'),
      value: $r('app.string.wifiSummaryConnected') },
    { key: $r('app.string.wifiInfoTitleIntensity'),
      value: $r('app.string.wifiSigIntensityBad') },
    { key: $r('app.string.wifiInfoTitleSpeed'),
      value: this.speed },
    { key: $r('app.string.wifiInfoTitleFrequency'),
      value: this.frequency },
    { key: $r('app.string.wifiInfoTitleEncryptMethod'),
      value: $r("app.string.wifiEncryptMethodOpen") }
  ];
  private buttons: string[] = [
    "取消", "删除", "配置IP"
  ]

  build() {
    Column() {
      Stack() {
        SettingBackground()
          .position({ x: 0, y: 0 })
          .height(ConfigData.WH_100_100)
          .width(ConfigData.WH_100_100)
        Row() {
          Column() {
            Row() {
              Text(this.apInfo.ssid)
                .focusable(false)
                .height('15%')
                .fontColor(Color.White)
                  //        .fontSize($r("app.float.font_20"))
                .fontSize("20lpx")
                .fontWeight(FontWeight.Bold)
                .textAlign(TextAlign.Start)
                .width(ConfigData.WH_100_100)
                .padding({
                  top: $r('app.float.distance_7'),
                  bottom: $r('app.float.distance_4')
                })
            }
            .focusable(false)

            List({ space: "10vp" }) {
              ForEach(this.dataList, (item) => {
                ListItem() {
                  ApInfoComponent({
                    label: item.key,
                    value: item.value
                  })
                }
                .focusable(false)
                .height(globalThis.screenHeight * 0.1)
              })
            }
            .margin({ top: $r('app.float.wh_value_4'), bottom: $r('app.float.wh_value_4') })

            Row({ space: globalThis.screenWidth * 0.02 }) {
              ForEach(this.buttons, (item, index) => {
                Column() {
                  Button({ type: ButtonType.Normal, stateEffect: false }) {
                    Text(item)
                      .fontSize("15lpx")
                      .fontColor(Color.White)
                      .fontWeight(FontWeight.Medium)
                      .textAlign(TextAlign.Center)
                      .focusable(true)
                  }
                  .height("100%")
                  .width("100%")
                  .backgroundColor("#1affffff")
                }
                .borderRadius("4lpx")
                .height(globalThis.screenHeight * 0.07)
                .width(globalThis.screenWidth * 0.15)
                .key("btn" + index.toString())
                .focusable(true)
                .border({
                  style: BorderStyle.Solid,
                  width: this.focusIndex == index ? '2lpx' : 0,
                  radius: '4lpx',
                  color: Color.White
                })
                .onFocus(() => {
                  this.focusIndex = index;
                  LogUtil.info(TAG + `apInfoDialog  focusIndex======>${this.focusIndex}`)
                })
                .margin({ left: $r("app.float.wh_value_4"), right: $r("app.float.wh_value_4") })
                .onKeyEvent((event) => {
                  if (!(event.keyCode == KeyCode.KEY_DPAD_LEFT && this.focusIndex > 0 || event.keyCode == KeyCode.KEY_DPAD_RIGHT && this.focusIndex < this.buttons.length || event.keyCode == KeyCode.KEY_ENTER)) {
                    focusControl.requestFocus("btn" + index.toString());
                    event.stopPropagation();
                  }
                  if (event.keyCode == KeyCode.KEY_ENTER && event.type == KeyType.Up) {
                    if (item == "取消") {
                      LogUtil.info(TAG + 'clicked ap info cancel');
                      router.back();
                    }

                    if (item == "删除") {
                      LogUtil.info(TAG + 'clicked ap info to disconnect');
                      if (this.isConnected) {
                        WifiModel.disconnectWiFi();
                      }
                      WifiModel.removeDeviceConfig(this.apInfo);
                      WifiModel.refreshApScanResults();
                      router.back();
                    }
                    if (item == "配置IP") {
                      router.push({
                        url: "pages/network/linkedWifi"
                      })
                    }
                    if (item == "连接") {
                      WifiModel.disconnectWiFi();
                      WifiModel.connectByDeviceConfig(this.apInfo);
                      WifiModel.refreshApScanResults();
                      router.back();
                    }
                  }
                })
              })
            }
            .onAppear(() => {
              focusControl.requestFocus("btn0")
            })
            .margin({
              top: $r('app.float.wh_value_16'),
              bottom: $r('app.float.wh_value_16'),
            })
            .borderRadius(lpx('wh_value_4'))
            .width(ConfigData.WH_100_100)
            .height(globalThis.screenHeight * 0.06)
          }
          .width(ConfigData.WH_50_100)
          .height(ConfigData.WH_100_100)
          .margin({ left: lpx('distance_12'), right: lpx('distance_12') })

          SettingItemDesLayout({ imageUrl: this.imageUrl, des: this.des })
            .width(ConfigData.WH_50_100)
            .height(ConfigData.WH_100_100)
        }
      }
    }
    .useSizeType({
      sm: { span: 4, offset: 0 },
      md: { span: 6, offset: 1 },
      lg: { span: 8, offset: 2 }
    })
    .width(ConfigData.WH_100_100)
    .height(ConfigData.WH_100_100)

  }

  async aboutToAppear() {
    // gen wifi signal level
    this.apInfo = router.getParams()["apInfo"];
    LogUtil.info(TAG + `connected dialog show:${JSON.stringify(this.apInfo)}}`)
    this.des = this.apInfo.ssid;
    let intensity = WifiModel.getSignalIntensity(this.apInfo);
    let encryptType = WifiModel.getEncryptMethod(this.apInfo);
    this.frequency = (this.apInfo.frequency / 1000).toFixed(1).toString() + 'GHz'

    this.isConnected = router.getParams()["isConnected"];
    if (this.isConnected) {
      this.buttons[this.buttons.length-1] = "配置IP";
      let linkInfo = await WifiModel.getLinkedWifiInfo();
      this.speed = linkInfo.linkSpeed + 'Mbps';
      this.frequency = (linkInfo.frequency / 1000).toFixed(1).toString() + 'GHz';
      this.dataList = [
        { key: $r('app.string.wifiInfoTitleStatus'),
          value: $r('app.string.wifiSummaryConnected') },
        { key: $r('app.string.wifiInfoTitleIntensity'),
          value: genWiFiIntensity(intensity) },
        { key: $r('app.string.wifiInfoTitleSpeed'),
          value: this.speed },
        { key: $r('app.string.wifiInfoTitleFrequency'),
          value: this.frequency },
        { key: $r('app.string.wifiInfoTitleEncryptMethod'),
          value: genWiFiEncryptMethod(encryptType) }
      ];
    } else {
      this.buttons[this.buttons.length-1] = "连接";
      this.dataList = [
        { key: $r('app.string.wifiInfoTitleStatus'),
          value: "未连接" },
        { key: $r('app.string.wifiInfoTitleIntensity'),
          value: genWiFiIntensity(intensity) },
        { key: $r('app.string.wifiInfoTitleFrequency'),
          value: this.frequency },
        { key: $r('app.string.wifiInfoTitleEncryptMethod'),
          value: genWiFiEncryptMethod(encryptType) }
      ];
    }
  }
}

@Component
export default struct ApInfoComponent {
  @Prop label: string;
  @Prop value: string;

  build() {
    Row() {
      Text(this.label)
        .fontSize("15lpx")
        .fontColor(Color.White)
        .fontWeight(FontWeight.Medium)
        .textAlign(TextAlign.Start)
        .focusable(false)
        .margin({ left: $r("app.float.wh_value_12") });

      Blank();

      Text(this.value)
        .fontSize("15lpx")
        .fontColor(Color.White)
        .fontWeight(FontWeight.Regular)
        .textAlign(TextAlign.End)
        .focusable(false)
        .margin({ right: $r("app.float.wh_value_12") });
    }
    .height(globalThis.screenHeight * 0.1)
    .borderRadius(lpx('wh_value_4'))
    .width(ConfigData.WH_100_100)
    .backgroundColor('#1affffff')
    .alignItems(VerticalAlign.Center);
  }
}


function genWiFiIntensity(intensityEnum: number) {
  if (intensityEnum === WiFiIntensityMap.GOOD) {
    return $r("app.string.wifiSigIntensityStrong");
  }

  if (intensityEnum === WiFiIntensityMap.WELL) {
    return $r("app.string.wifiSigIntensityWell");
  }

  if (intensityEnum === WiFiIntensityMap.NORMAL) {
    return $r("app.string.wifiSigIntensityNormal");
  }

  if (intensityEnum === WiFiIntensityMap.BAD) {
    return $r("app.string.wifiSigIntensityBad");
  }
  return $r("app.string.wifiSigIntensityBad");
}

function genWiFiEncryptMethod(encryptEnum: number) {
  if (encryptEnum === WiFiEncryptMethodMap.OPEN) {
    return $r("app.string.wifiEncryptMethodOpen");
  }

  if (encryptEnum === WiFiEncryptMethodMap.WEP) {
    return $r("app.string.wifiEncryptMethodWEP");
  }

  if (encryptEnum === WiFiEncryptMethodMap.WPA) {
    return $r("app.string.wifiEncryptMethodWPA");
  }

  if (encryptEnum === WiFiEncryptMethodMap.WPA2) {
    return $r("app.string.wifiEncryptMethodWPA2");
  }

  return $r("app.string.wifiEncryptMethodOpen");
}


