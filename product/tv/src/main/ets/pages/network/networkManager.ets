/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { SettingBackground } from '../../default/layout/SettingBackground';
import { Ratio } from '../../common/ratio';
import router from '@ohos.router';
import prompt from '@ohos.prompt';
import lpx from '../../common/lpx'
import { SettingListTvComponent } from '../../default/layout/SettingListTvComponent'
import { SettingItemStyle } from '../../common/constants/StyleConstants';
import LogUtil from '../../../../../../../common/utils/src/main/ets/default/baseUtil/LogUtil';
import ConfigData from '../../../../../../../common/utils/src/main/ets/default/baseUtil/ConfigData';
import { SettingItemDesLayout } from '../../default/layout/SettingItemDesLayout'
import { SettingItemTvComponent } from '../../default/layout/SettingItemTvComponent'
import { SettingItemTvModel } from '../../default/model/SettingItemTvModel'
import { KeyCode } from '../../common/constants/KeyCode';

/**
 * 网络管理
 */
@Entry
@Component
struct NetworkManager {

  @State imageUrl: string = Ratio.resName('icon_net_manage')
  @State des: string = '设置网络优先级'
  @State settingList: SettingItemTvModel[] = []

  build() {

    Column() {
      Stack() {
        SettingBackground()
          .position({ x: 0, y: 0 })
          .height(ConfigData.WH_100_100)
          .width(ConfigData.WH_100_100)

        Row() {
          SettingListTvComponent({
            settingList: this.settingList,
            title: "网络管理",
            onClickHandle: (item) => {

            },
            onKeyEventHandle: (item, event) => {
                if(event.type==KeyType.Up){
                  if(event.keyCode==KeyCode.KEY_ENTER){
                     AppStorage.SetOrCreate("netWork",item.name);
                  }
                }
            }
          })
            .width(ConfigData.WH_50_100)
            .height(ConfigData.WH_100_100)
            .margin({ left: lpx('distance_12'), right: lpx('distance_12') })

          SettingItemDesLayout({ imageUrl: this.imageUrl, des: this.des })
            //          .backgroundColor(Color.Red)
            .width(ConfigData.WH_50_100)
            .height(ConfigData.WH_100_100)

        }
      }
    }
    .useSizeType({
      sm: { span: 4, offset: 0 },
      md: { span: 6, offset: 1 },
      lg: { span: 8, offset: 2 }
    })
//    .backgroundColor('#54575d')
    .width(ConfigData.WH_100_100)
    .height(ConfigData.WH_100_100)

  }

  aboutToAppear() {
    let item1=new SettingItemTvModel();
    item1.name="无线网络";
    item1.style=SettingItemStyle.singleChoose;

    let item2=new SettingItemTvModel();
    item2.style=SettingItemStyle.singleChoose;
    item2.name="有线网络";

    let net=AppStorage.Get("netWork");
    if(net=="无线网络"){
       item1.setIsSelect(true);
    }else{
      item2.setIsSelect(true);
    }
    this.settingList.push(item1, item2);
  }

}

