/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import router from '@ohos.router';
import { SettingItemStyle } from '../../common/constants/StyleConstants';
import LogUtil from '../../../../../../../common/utils/src/main/ets/default/baseUtil/LogUtil';
import ConfigData from '../../../../../../../common/utils/src/main/ets/default/baseUtil/ConfigData';
import { SettingItemDesLayout } from '../../default/layout/SettingItemDesLayout'
import { SettingItemTvComponent } from '../../default/layout/SettingItemTvComponent'
import { SettingItemTvModel } from '../../default/model/SettingItemTvModel'
import { SettingListTvComponent } from '../../default/layout/SettingListTvComponent'
import { KeyCode } from '../../common/constants/KeyCode';
import lpx from '../../common/lpx'
import audio from '@ohos.multimedia.audio';
import { volumeOutputModel } from '../../model/volumeoutputImpl/VolumeOutputModel'
import { Ratio } from '../../common/ratio';
import { SettingBackground } from '../../default/layout/SettingBackground';

/**
 *次世代降级输出
 */
@Entry
@Component
struct NgDegradeOutput {
  @State listSpaces: string = '5vp';

  build() {
    Column() {
      Column() {
        EntryComponent({ listSpaces: this.listSpaces })
      }
      .width(ConfigData.WH_100_100)
      .height(ConfigData.WH_100_100)
    }
  }

  aboutToAppear() {
    LogUtil.info('settings DegradeOutput aboutToAppear enter');
    LogUtil.info('settings DegradeOutput aboutToAppear end');
  }
}

@Component
struct EntryComponent {
  private listSpaces: string = '1vp';
  @State imageUrl: string = Ratio.resName('icon_volume')
  @State des: string = '声音输出选项'
  @State settingList: SettingItemTvModel[] = []
  @State focusIndex: number = -1
  ngDegradeOption: Map<string, number> = new Map()
  @StorageLink("degradeOption") degradeOption: string = "自动"

  aboutToAppear() {
    LogUtil.info('settings DegradeOutput EntryComponent aboutToAppear in');
    this.initNgDegradeListItem();
    this.initNgDegradeOption();
  }

  initNgDegradeListItem() {
    let items: string[] = ["透传5.1", "透传7.1", "自动"];
    items.forEach((name) => {
      let model: SettingItemTvModel = new SettingItemTvModel();
      model.name = name;
      if (name == this.degradeOption) {
        model.setIsSelect(true);
      }
      model.style = SettingItemStyle.singleChoose;
      this.settingList.push(model);
    })
  }

  initNgDegradeOption() {
    this.ngDegradeOption.set("自动", 2);
    this.ngDegradeOption.set("透传7.1", 1);
    this.ngDegradeOption.set("透传5.1", 3);
  }

  build() {
    Stack() {
      SettingBackground()
        .position({ x: 0, y: 0 })
        .height(ConfigData.WH_100_100)
        .width(ConfigData.WH_100_100)

      Row() {
        SettingListTvComponent({
          settingList: this.settingList,
          title: "次世代降级输出",
          onClickHandle: (item) => {

          },
          onKeyEventHandle: (item, event) => {
            if (event.keyCode == KeyCode.KEY_ENTER&&event.type==KeyType.Up) {
              AppStorage.SetOrCreate("degradeOption", item.name);
              volumeOutputModel.setOutNgenerationMode(this.ngDegradeOption.get(item.name));
            }
          }
        })
          .width(ConfigData.WH_50_100)
          .height(ConfigData.WH_100_100)
          .margin({ left: lpx('distance_12'), right: lpx('distance_12') })

        SettingItemDesLayout({ imageUrl: this.imageUrl, des: this.des })
          .width(ConfigData.WH_50_100)
          .height(ConfigData.WH_100_100)
      }
    }
  }
}
