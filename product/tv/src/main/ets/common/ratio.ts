import LogUtil from '../../../../../../common/utils/src/main/ets/default/baseUtil/LogUtil';

const TAG = "Ratio";

export class Ratio {
    static resName(res: string): string {
        let r = res
//        if (globalThis.resolution == '4KUI') {
//            r = r + '_4x'
//        }
        LogUtil.info(TAG + ' resName: ' + r);
        return `resources/base/media/${ r }.svg`
//        return `resources/base/media/${ r }.png`
    }
}