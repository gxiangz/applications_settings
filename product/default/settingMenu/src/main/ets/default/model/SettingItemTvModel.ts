import BaseModel from '../../../../../../../../common/utils/src/main/ets/default/model/BaseModel';
import LogUtil from '../../../../../../../../common/utils/src/main/ets/default/baseUtil/LogUtil';
import { SettingItemStyle } from '../../common/constants/StyleConstants'

const TAG = `SettingItemTvModel`;

export class SettingItemTvModel extends BaseModel {
    name: string = '' // 设置名

    chooseDes: string = '' // 设置选项选择后的描述

    value: string = '' // 设置值

    isOn: boolean = false // 开关状态

    isSelect: boolean = false // 选中状态

    style: SettingItemStyle = SettingItemStyle.default // item类型

    pageUrl: string = '' // 跳转路径

    imageUrl: string = '' // 描述图片路径

    description: string = '' // 描述

    constructor() {
        super();
        LogUtil.info(`${TAG} init`);
    }

    setIsOn = (isOn: boolean)  => {
        this.isOn = isOn
    }
    setIsSelect =  (isSelect: boolean) => {
        this.isSelect = isSelect
    }
    setValue = (value: string) => {
        this.value = value;
    }
    setChooseDes = (value: string) => {
        this.chooseDes = value
    }
}