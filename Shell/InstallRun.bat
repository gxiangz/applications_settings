set settings_replace_folder=\product\tv\build\default\outputs\default\tv-default-signed.hap

set settings_menu_replace_folder=\product\default\settingMenu\build\default\outputs\default\tv_menu-default-signed.hap

set hdc_std=hdc_std.exe

%hdc_std% shell mount -o remount,rw /

%hdc_std% shell rm -rf /data/*

%hdc_std% file send %settings_replace_folder% /system/app/com.ohos.settings/Settings.hap

%hdc_std% file send %settings_menu_replace_folder% /system/app/com.ohos.settings/Settings_Menu.hap

%hdc_std% shell reboot