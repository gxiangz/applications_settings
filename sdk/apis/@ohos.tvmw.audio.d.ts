/*
* Copyright (c) 2021-2022 Huawei Device Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

import {ErrorCallback, AsyncCallback, Callback} from './basic';
import Context from './@ohos.ability';

/**
 * @name audio
 * @SysCap SystemCapability.Multimedia.Audio
 * @devices phone, tablet, wearable, car
 * @since 7
 */
declare namespace tvMWAudio {
  /**
   * Setting the Data Format of the HDMI Output Interface.
   * @return Returns true if the setting is successful,returns false otherwise.
   * @since 9
   * @SysCap SystemCapability.Multimedia.Audio
   */
  function setOutputHDMIMode(mode: AudioHDMIMode, callback: AsyncCallback<boolean>): void;

  /**
   * Setting the Data Format of the HDMI Output Interface.
   * @since 9
   * @return Returns true if the setting is successful,returns false otherwise.
   * @SysCap SystemCapability.Multimedia.Audio
   */
  function setOutputHDMIMode(mode: AudioHDMIMode): Promise<boolean>;

  /**
   * Obtains the data format of the HDMI output interface.
   * @since 9
   * @return Returns true if the setting is successful,returns false otherwise.
   * @SysCap SystemCapability.Multimedia.Audio
   */
  function getOutputHDMIMode(callback: AsyncCallback<number>): void;
  function getOutputHDMIMode(): Promise<number>;
  
  /**
   * Sets the data format (compressed or PCM) of the S/PDIF output interface.
   * @return Returns true if the setting is successful,returns false otherwise. 
   * @since 9
   * @SysCap SystemCapability.Multimedia.Audio
   */
  function setOutputSPDIFMode(mode: AudioSPDIFMode, callback: AsyncCallback<void>): void;

  /**
   * Sets the data format (compressed or PCM) of the S/PDIF output interface.
   * @return Returns true if the setting is successful,returns false otherwise. 
   * @since 9 
   * @SysCap SystemCapability.Multimedia.Audio
   */
  function setOutputSPDIFMode(mode: AudioSPDIFMode): Promise<void>;
  
    /**
   * Obtains the data format of the SPDIF output interface.
   * @since 9
   * @SysCap SystemCapability.Multimedia.Audio
   */
  function getOutputSPDIFMode(callback: AsyncCallback<number>): void;
  function getOutputSPDIFMode(): Promise<number>;

  /**
   * @return Returns true if the setting is successful,returns false otherwise.
   * @since 9
   * @SysCap SystemCapability.Multimedia.Audio
   */
  function setOutNgenerationMode(mode: AudioNgenerationMode, callback: AsyncCallback<boolean>): void;

    /** 
    * @since 9
    * @return Returns true if the setting is successful,returns false otherwise.
    * @SysCap SystemCapability.Multimedia.Audio
    */
  function setOutNgenerationMode(mode: AudioNgenerationMode): Promise<boolean>;

  /**
   * @since 9
   * @SysCap SystemCapability.Multimedia.Audio
   */
  function getOutNgenerationMode(callback: AsyncCallback<number>): void;
  function getOutNgenerationMode(): Promise<number>;

  enum AudioHDMIMode {
    /**
     * Close mode.
     */
    HDMI_MODE_Close = 0,

    /**
     * AutoNegotiation mode.
     */
    HDMI_MODE_AUTO = 1,

      /**
      * outputting a decoded signal
      */
    HDMI_MODE_LPCM = 2,

    /**
     * Output original signal.
     */
    HDMI_MODE_RAW = 3,
  }

  enum AudioSPDIFMode {
    /**
     * Compressed audio is decoded by the receiving terminal.
     */
    SPDIF_MODE_LPCM = 0,

    /**
     * Compressed audio is decoded by an external decoding device.
     */
    SPDIF_MODE_RAW = 1,
  }
  
  enum AudioNgenerationMode {
    /**
     * AutoNegotiation mode.
     */
    NGENERATION_MODE_AUTO = 0,

    /**
     * 7.1 Transparent transmission.
     */
    NGENERATION_MODE_RAW = 1,

      /**
     * 5.1 Transparent transmission.
     */
    NGENERATION_MODE_HBR2LBR = 2,
  }
}

export default tvSettingAudio;