/*
* Copyright (C) 2022 Huawei Device Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

import { AsyncCallback } from './basic';

/**
 * displaySetting
 * @syscap SystemCapability.Graphic.DisplaySetting
 */
 declare namespace displaySetting {

/**
     * Sets the saturation (chrominance) of the video output. This setting does not apply to a single video output unit. 
     * It takes effect for all video output units at the same time.
     * @devices tv, phone, tablet, wearable
     * @param value –number: Indicates the saturation (chrominance). The value ranges from 0 to 100 in ascending order.
     * @return Returns true if the setting is successful,returns false otherwise.
     */
 function setOutputSaturation (value : number, callback: AsyncCallback<boolean>): void;
 /**
   * Sets the saturation (chrominance) of the video output. This setting does not apply to a single video output unit. 
   * It takes effect for all video output units at the same time.
   * @devices tv, phone, tablet, wearable
   * @param value –number: Indicates the saturation (chrominance). The value ranges from 0 to 100 in ascending order.
   * @return Returns true if the setting is successful,returns false otherwise.
   */
 function setOutputSaturation (value: number): Promise<boolean>;
 /**
   * Obtains the saturation (chrominance) of the video output.
   * @devices tv, phone, tablet, wearable
   * @return Number type, indicating the saturation (chrominance) of the video output. The value ranges from 0 to 100 in ascending order.
   */
 function getOutputSaturation (callback: AsyncCallback<number>): void;
 /**
   * Obtains the saturation (chrominance) of the video output.
   * @devices tv, phone, tablet, wearable
   * @return Number type, indicating the saturation (chrominance) of the video output. The value ranges from 0 to 100 in ascending order.
   */
 function getOutputSaturation (): number;
 /**
   * Sets the contrast of the video output. This setting does not apply to a single video output unit. 
   * It takes effect for all video output units at the same time.
   * @devices tv, phone, tablet, wearable
   * @param value –number: indicates the contrast of the video output. The value ranges from 0 to 100 in ascending order.
   * @return Returns true if the setting is successful,returns false otherwise.
   */
 function setOutputContrast (value : number, callback: AsyncCallback<boolean>): void;
 /**
   * Sets the contrast of the video output. This setting does not apply to a single video output unit. 
   * It takes effect for all video output units at the same time.
   * @devices tv, phone, tablet, wearable
   * @param value –number: indicates the contrast of the video output. The value ranges from 0 to 100 in ascending order.
   * @return Returns true if the setting is successful,returns false otherwise.
   */
 function setOutputContrast (value: number): Promise<boolean>;
 /**
   * Obtains the contrast of the video output.
   * @devices tv, phone, tablet, wearable
   * @return Number type, indicating the contrast of the video output. The value ranges from 0 to 100 in ascending order.
   */
 function getOutputContrast (callback: AsyncCallback<number>): void;
 /**
   * Obtains the contrast of the video output.
   * @devices tv, phone, tablet, wearable
   * @return Number type, indicating the contrast of the video output. The value ranges from 0 to 100 in ascending order.
   */
 function getOutputContrast (): number;
 /**
   * Sets the hue of the video output. This setting does not apply to a single video output unit. 
   * It takes effect for all video output units at the same time.
   * @devices tv, phone, tablet, wearable
   * @param value –number: indicates the hue of the video output. The value ranges from 0 to 100 in ascending order.
   * @return Returns true if the setting is successful,returns false otherwise.
   */
 function setDisplayHue (value : number, callback: AsyncCallback<boolean>): void;
 /**
   * Sets the hue of the video output. This setting does not apply to a single video output unit. 
   * It takes effect for all video output units at the same time.
   * @devices tv, phone, tablet, wearable
   * @param value –number: indicates the hue of the video output. The value ranges from 0 to 100 in ascending order.
   * @return Returns true if the setting is successful,returns false otherwise.
   */
 function setDisplayHue (value: number): Promise<boolean>;
 /**
   * Obtains the hue of the video output.
   * @devices tv, phone, tablet, wearable
   * @return Number type, indicating the hue of the video output. The value ranges from 0 to 100 in ascending order.
   */
 function getDisplayHue (callback: AsyncCallback<number>): void;
 /**
   * Obtains the hue of the video output.
   * @devices tv, phone, tablet, wearable
   * @return Number type, indicating the hue of the video output. The value ranges from 0 to 100 in ascending order.
   */
 function getDisplayHue (): number;
 /**
   * Set the HDR mode.
   * @devices tv, phone, tablet, wearable
   * @param type–number,0-HDRTYPE_SDR,1-HDRTYPE_DOLBY,2-HDRTYPE_HDR10,3-CHDRTYPE_AUTO
   * @return Returns true if the setting is successful,returns false otherwise.
   */
 function setHDRType (value : number, callback: AsyncCallback<boolean>): void;
 /**
   * Set the HDR mode. 
   * @devices tv, phone, tablet, wearable
   * @param type–number,0-HDRTYPE_SDR,1-HDRTYPE_DOLBY,2-HDRTYPE_HDR10,3-CHDRTYPE_AUTO
   * @return Returns true if the setting is successful,returns false otherwise.
   */
 function setHDRType (value: number): Promise<boolean>;
 /**
   * Obtains the HDR mode.
   * @devices tv, phone, tablet, wearable
   * @return Number type. 0: HDRTYPE_SDR; 1: HDRTYPE_DOLBY; 2: HDRTYPE_HDR10; 3: CHDRTYPE_AUTO.
   */
 function getHDRType (callback: AsyncCallback<number>): void;
 /**
   * Obtains the HDR mode.
   * @devices tv, phone, tablet, wearable
   * @return Number type. 0: HDRTYPE_SDR; 1: HDRTYPE_DOLBY; 2: HDRTYPE_HDR10; 3: CHDRTYPE_AUTO.
   */
 function getHDRType (): number;
 /**
   * Sets the video output standard. The SD output unit and HD output unit need to be set separately.
   * @devices tv, phone, tablet, wearable
   * @param device - number type, indicating the video output unit. standard-number,
   * which indicates the video output standard. The SD output unit can select only the SD standard, and the HD output unit can select only the HD standard.
   * @return Returns true if the setting is successful,returns false otherwise.
   */
 function setOutputStandard (value : string, callback: AsyncCallback<boolean>): void;
  /**
    * Sets the video output standard. The SD output unit and HD output unit need to be set separately.
    * @devices tv, phone, tablet, wearable
    * @param device - number type, indicating the video output unit. standard-number, 
    * which indicates the video output standard. The SD output unit can select only the SD standard, and the HD output unit can select only the HD standard.
    * @return Returns true if the setting is successful,returns false otherwise.
    */
 function setOutputStandard (value: string): Promise<boolean>;
  /**
    * Obtains the video output standard.
    * @devices tv, phone, tablet, wearable
    * @params device –number type, indicating the video output channel. The value can be VOUT_SD or VOUT_HD.
    * @return Number type, indicating the video output standard.
    */
 function getOutputStandard (callback: AsyncCallback<Array<string>>): void;
  /**
    * Obtains the video output standard.
    * @devices tv, phone, tablet, wearable
    * @params device –number type, indicating the video output channel. The value can be VOUT_SD or VOUT_HD.
    * @return Number type, indicating the video output standard.
    */
 function getOutputStandard (): Array<string>;

 function getCurOutputStandard (): string;
 /* 
  * Enables the HDMI adaptation function.
  * @devices tv, phone, tablet, wearable
  * @return Returns true if the setting is successful,returns false otherwise.
  */
 function enableHDMIAdaptive (callback: AsyncCallback<boolean>): void;
 /* 
  * Enables the HDMI adaptation function.
  * @devices tv, phone, tablet, wearable
  * @return Returns true if the setting is successful,returns false otherwise.
  */
 function enableHDMIAdaptive (): Promise<boolean>;

 function isHDMIAdaptive (): boolean;
 /* 
  * Disables the HDMI adaptation function.
  * @devices tv, phone, tablet, wearable
  * @return Returns true if the setting is successful,returns false otherwise.
  */
 function disableHDMIAdaptive (callback: AsyncCallback<boolean>): void;
 /* 
  * Disables the HDMI adaptation function.
  * @devices tv, phone, tablet, wearable
  * @return Returns true if the setting is successful,returns false otherwise.
  */
 function disableHDMIAdaptive (): Promise<boolean>;
 /* 
  * Enables the CEC function of the HDMI.
  * @devices tv, phone, tablet, wearable
  * @return Returns true if the setting is successful,returns false otherwise.
  */
 function enableHDMICEC (callback: AsyncCallback<boolean>): void;
 /* 
  * Enables the CEC function of the HDMI.
  * @devices tv, phone, tablet, wearable
  * @return Returns true if the setting is successful,returns false otherwise.
  */
 function enableHDMICEC (): Promise<boolean>;

 function isHDMICEC (): boolean;
 /* 
  * Disables the CEC function of the HDMI.
  * @devices tv, phone, tablet, wearable
  * @return Returns true if the setting is successful,returns false otherwise.
  */
 function disableHDMICEC (callback: AsyncCallback<boolean>): void;
 /* 
  * Disables the CEC function of the HDMI.
  * @devices tv, phone, tablet, wearable
  * @return Returns true if the setting is successful,returns false otherwise.
  */
 function disableHDMICEC (): Promise<boolean>;
 /* 
  * The external HDMI CEC device can be remotely controlled to enter the standby mode.
  * @devices tv, phone, tablet, wearable
  * @return Returns true if the setting is successful,returns false otherwise.
  */
 function setHDMICECRemote (callback: AsyncCallback<boolean>): void;
 /* 
  * The external HDMI CEC device can be remotely controlled to enter the standby mode.
  * @devices tv, phone, tablet, wearable
  * @return Returns true if the setting is successful,returns false otherwise.
  */
 function setHDMICECRemote (): Promise<boolean>;

 function enableHDMICECRemote(): Promise<boolean>;
 function enableHDMICECRemote(callback: AsyncCallback<boolean>): void;

 function disableHDMICECRemote(): Promise<boolean>;
 function disableHDMICECRemote(callback: AsyncCallback<boolean>): void;

 function isHDMICECRemote(): boolean;

 /*
  * Enables the HDCP function of the HDMI.
  * @devices tv, phone, tablet, wearable
  * @return Returns true if the setting is successful,returns false otherwise.
  */
 function enableHDMIHDCP (callback: AsyncCallback<boolean>): void;
 /* 
  * Enables the HDCP function of the HDMI.
  * @devices tv, phone, tablet, wearable
  * @return Returns true if the setting is successful,returns false otherwise.
  */
 function enableHDMIHDCP (): Promise<boolean>;
 /* 
  * Disables the HDCP function of the HDMI.
  * @devices tv, phone, tablet, wearable
  * @return Returns true if the setting is successful,returns false otherwise.
  */
 function disableHDMIHDCP (callback: AsyncCallback<boolean>): void;

 function isHDMIHDCP (): boolean;
 /*
  * Disables the HDCP function of the HDMI.
  * @devices tv, phone, tablet, wearable
  * @return Returns true if the setting is successful,returns false otherwise.
  */
 function disableHDMIHDCP (): Promise<boolean>;
 /* 
  * Setting the video display area.
  * @devices tv, phone, tablet, wearable
  * @params x,y - Horizontal and vertical coordinates of the vertex,width,height - Width and height of the display area.
  * @return Returns true if the setting is successful,returns false otherwise.
  */
 function setVideoDisplayArea (x:number,y:number,width:number,height:number,callback: AsyncCallback<boolean>): void;
 /* 
  * Setting the video display area
  * @devices tv, phone, tablet, wearable
  * @params x,y - Horizontal and vertical coordinates of the vertex,width,height - Width and height of the display area.
  * @return Returns true if the setting is successful,returns false otherwise.
  */
 function setVideoDisplayArea (x:number,y:number,width:number,height:number): Promise<boolean>;
 }
export default displaySetting;